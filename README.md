[![](/img/36a8cce1.png)](https://www.sarfraztek.com/)

# Brought to you by 🕵️‍

[![](img/ac15001b.png)](https://www.tekdeliver.com/) [![](/img/e22ba939.png)](https://www.opensecurityalliance.org/)

# What is this ?  🤔

You were redirected here most probably from WW. This repository will contain that information and related assets. That information is in the section below.

# Cybersecurity Fundamentals Free Training

This is our training deck on this subject -  [ClickMe](https://tekdeliverdeks.gitlab.io/tdfcs/)

# The Stuff  😎 

The following table contains the links to the subject details.

No. | Date | Comment | URL | Severity Level |
--|--|--|--|--
1 | Thu-13-Jun-2019 | Evernote Vulnerability | [VIEW](https://gitlab.com/msatek/an0nfiles/wikis/Evernote-Web-Clipper-Vulnerability) | Medium 😐
2 | Tue-25-Jun-2019 | Cyberwarfare Wiper Malware Analysis | [VIEW](https://gitlab.com/msatek/an0nfiles/wikis/CyberWarFare-Wiper-Malware) | HIGH 🥵
3 | Wed-26-Jun-2019 | MAC OS Gatekeeper Bypass - Contested/Controversial | [VIEW](https://gitlab.com/msatek/an0nfiles/wikis/MAC-OS-Gatekeeper-Zero-Day-Vulnerability-Zero-Day-NO-SOLUTION-AVAILABLE) | Medium 😐
4 | Mon-1-Jul-2019 | MAC OS/X CrecentCore - Vulnerability | [VIEW](https://gitlab.com/msatek/an0nfiles/wikis/OSX/Crecentcore-Malware) | Medium 😐
5 | SUN-3-Oct-2019 | Largest Indian Bank Credit Card Data Stolen | [VIEW](https://gitlab.com/msatek/an0nfiles/-/wiki_pages/%F0%9F%87%AE%F0%9F%87%B3%E2%82%B9-Largest-Indian-Bank-Credit-Card-Data-Stolen-%5BDEVELOPING-STORY%5D) | HIGH 🥵


# About Us 🤗 

[![](/img/81d2fc6b.png)](https://www.tekdeliver.com/)

## Relevant Links 

1. [Our Training Repo](https://gitlab.com/msatek/osacapn) - With ongoing training sessions.  

